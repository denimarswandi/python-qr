import pyqrcode


def generate_qr(number_qr):
    data = number_qr
    data_png = pyqrcode.create(data)
    data_png.png('{}.png'.format(number_qr), scale=8)


if __name__ == '__main__':
  for i in range(200):
    i_fix = str(i+1)
    #data = ''
    if len(i_fix)==1:
      data = '00{}'.format(i_fix)
    elif len(i_fix)==2:
      data = '0{}'.format(i_fix)
    else:
      data = i_fix
    generate_qr(data)
